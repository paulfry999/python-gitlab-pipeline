default: prereqs main

prereqs:
	@pip3 install -r requirements.txt

main:
	@python3 main.py
