#!/usr/bin/env python3
"""
Boilerplate Python script
Python Version: 3.7
"""

__author__ = "Paul Fry"
__version__ = "0.1"

import os
import sys
from datetime import datetime
from time import time
import logzero
from logzero import logger

#import custom modules

log_fmt = '%(color)s[%(levelname)1.1s  %(asctime)s  %(module)s:%(lineno)d]%(end_color)s %(message)s'
formatter = logzero.LogFormatter(fmt=log_fmt, datefmt='%d-%m-%Y %I:%M:%S %p')
logzero.setup_default_logger(formatter=formatter)

working_dir = os.getcwd()

current_dt_obj = datetime.now()
current_date_str = current_dt_obj.strftime('%d-%m-%Y')
current_time_str = current_dt_obj.strftime('%H:%M:%S')
#can use 'current_dt_obj' to get other date parts. E.g. 'current_dt_obj.year'

def main():
    """ Main entry point of the app """
    start_time = time()
    logger.debug("Main() function called")
    logger.debug("Finished main() function in %s seconds", round(time() - start_time, 2))

if __name__ == '__main__':
    """ This is executed when run from the command line """
    main()
    